<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');
Route::get('/nlp','NLPController@beginClassification');
Route::get('/forecast','WeatherClassification@classification');
Route::get('/demo_api',function (){
    $response=file_get_contents('https://api.apixu.com/v1/current.json?key=5559d3649f074374a20135950190405&q=SaiGon');
    $array= json_decode($response);
    var_dump($array);
    var_dump($array->location->name);
    var_dump($array->location->country);
    var_dump($array->current->temp_c);
    var_dump($array->current->condition->text);
});