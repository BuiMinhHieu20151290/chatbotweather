<?php
use App\Http\Controllers\BotManController;

$botman = resolve('botman');

$botman->hears('Hi', function ($bot) {
    $bot->startConversation(new \App\Conversations\GreetConversation());
});
$botman->hears('Start conversation', BotManController::class.'@startConversation');