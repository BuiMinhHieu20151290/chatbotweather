<?php

namespace App\Conversations;

use App\ClassificationModel;
use App\Http\Controllers\WeatherClassification;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;

class GreetConversation extends Conversation
{
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function greet(){

        $this->ask('Hello, I am Chatbot Weather. Do you want to know the weather information ?', function (Answer $answer){
            $text= $answer->getText();
            $classification= new ClassificationModel();
            $result= $classification->yesNoQuestion($text);
            if($result=='Yes'){
                $this->askLever2();
            }else{
                $this->say('Thanks you for using ChatbotWeather');
            }
        });
    }
    public function askLever2(){
        $this->ask('Give me your address or activity what do you want ',function (Answer $answer){
            $pp = new ClassificationModel();
            $clasifyQs= $pp->classifyQuestion($answer->getText());
            if($clasifyQs=='Activity'){
                $this->say('Today weather is very nice. You can walk arounf park');
            }else{
                $address= $answer->getText();
                $result= file_get_contents('https://api.apixu.com/v1/current.json?key=5559d3649f074374a20135950190405&q='.$address);
                $response= json_decode($result);
                $this->say('Address: '.$response->location->name);
                $this->say('Country: '.$response->location->country);
                $this->say('Temperature: '.$response->current->temp_c);
                $this->say('Condition: '.$response->current->condition->text);
            }
            $this->continueAsk();
        });
    }
    public function continueAsk(){
        $this->ask('Do you want to ask',function (Answer $answer){
            $model = new ClassificationModel();
            $result= $model->continueOrStop($answer->getText());
            if($result=='Yes'){
                $this->greet();
            }else{
                $this->say('Thank you for using chatbot, see you again');
            }
        });
    }
    public function run()
    {
        //
        $this->greet();
    }
}
