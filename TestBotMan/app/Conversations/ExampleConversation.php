<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use NlpTools\Tokenizers\WhitespaceTokenizer;
use NlpTools\Documents\TrainingSet;
use NlpTools\FeatureFactories\DataAsFeatures;
use NlpTools\Documents\TokensDocument;
class ExampleConversation extends Conversation
{
    /**
     * First question
     */
    public function askReason()
    {
        $question = Question::create("Huh - you woke me up. What do you need?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Tell a joke')->value('joke'),
                Button::create('Give me a fancy quote')->value('quote'),
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'joke') {
                    $joke = json_decode(file_get_contents('http://api.icndb.com/jokes/random'));
                    $this->say($joke->value->joke);
                } else {
                    $this->say(Inspiring::quote());
                }
            }
        });
    }

    /**
     * Start the conversation
     */
    public function nlp(){
        $training = array(
            array('Greet','Hello'),
            array('Greet','Hi'),
            array('Greet','Good morning'),
            array('Greet','Good afternoon'),
            array('Greet','Hello, my name is Hieu'),
            array('Greet','Hi, Ha. Nice to meet you'),
            array('Greet','Good morning, Can I help you'),
            array('Greet','Hi, Nam. Nice to meet you'),
            array('Weather','What weather today ?'),
            array('Weather','What temperature to day ?'),
            array('Weather','I can go swimming'),
            array('Weather','Can i can play soccer in yard'),
            array('Weather','I play basketball'),
            array('Weather','I can go swimming'),
            array('Weather','Is today is cold'),
            array('Weather','Is to day is hot ?'),
            array('Shopping','This is very beautiful. I can buy it'),
            array('Shopping','I can buy this gift'),
            array('Shopping','How much is your cost')
        );
        $tset = new TrainingSet(); // will hold the training documents
        $tok = new WhitespaceTokenizer(); // will split into tokens
        $ff = new DataAsFeatures(); // see features in documentation
        foreach ($training as $d)
        {
            $tset->addDocument(
                $d[0], // class
                new TokensDocument(
                    $tok->tokenize($d[1]) // The actual document
                )
            );
        }
        $this->say($tset);
    }
    public function run()
    {
        $this->askReason();
    }
}
