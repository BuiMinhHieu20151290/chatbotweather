<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use NlpTools\Tokenizers\WhitespaceTokenizer;
use NlpTools\Models\FeatureBasedNB;
use NlpTools\Documents\TrainingSet;
use NlpTools\Documents\TokensDocument;
use NlpTools\FeatureFactories\DataAsFeatures;
use NlpTools\Classifiers\MultinomialNBClassifier;
class NLPController extends Controller
{
    //
    public function beginClassification($answer){
        $training = array(
            array('Greet','Hi'),
            array('Greet','Hello'),
            array('Greet','My name is Hieu. Nice to meet you'),
            array('Greet','Good morning'),
            array('Greet','Good afternoon'),
            array('Greet','Hello, everyone. Nice to meet you'),
            array('Weather','What is the weather today'),
            array('Weather','Today is hot'),
            array('Weather','Temperature today is 30C'),
            array('Weather','The sky have many foogy'),
            array('Weather','I want to shopping'
            )
        );
        // and another for evaluating
        $testing = array();
        array_push($testing,$answer);

        $tset = new TrainingSet(); // will hold the training documents
        $tok = new WhitespaceTokenizer(); // will split into tokens
        $ff = new DataAsFeatures(); // see features in documentation

        // ---------- Training ----------------
        foreach ($training as $d)
        {
            $tset->addDocument(
                $d[0], // class
                new TokensDocument(
                    $tok->tokenize($d[1]) // The actual document
                )
            );
        }

        $model = new FeatureBasedNB(); // train a Naive Bayes model
        $model->train($ff,$tset);


        // ---------- Classification ----------------
        $cls = new MultinomialNBClassifier($ff,$model);
        $correct = 0;
        foreach ($testing as $d)
        {
            // predict if it is spam or ham
            $prediction = $cls->classify(
                array('Greet','Weather'), // all possible classes
                new TokensDocument(
                    $tok->tokenize($d) // The document
                )
            );
            return $prediction;
        }
//        $jsonfile = file_get_contents("https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=c249b0f041bed32a33a1453c9c946dac");
//        echo $jsonfile;
    }
}
