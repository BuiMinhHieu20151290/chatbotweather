<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use NlpTools\Tokenizers\WhitespaceTokenizer;
use NlpTools\Models\FeatureBasedNB;
use NlpTools\Documents\TrainingSet;
use NlpTools\Documents\TokensDocument;
use NlpTools\FeatureFactories\DataAsFeatures;
use NlpTools\Classifiers\MultinomialNBClassifier;
class ClassificationModel extends Model
{
    //
    public function demo(){
        return 'Hello';
    }
    public function  yesNoQuestion($answer){
        $training = array(
            array('Yes', 'I want to know'),
            array('Yes',' Oh good. It is very wonderful'),
            array('Yes','Yes, I thinks it is neccesary'),
            array('Yes','I want to know it'),
            array('Yes','Oh yes, give me this information'),
            array('Yes','Ok, give me'),
            array('Yes','Good job. I need to it'),
            array('Yes','I needt it'),
            array('No','No I dont need it'),
            array('No','No, I knowed it'),
            array('No','Thanks. But I dont need it'),
            array('No','No,It is very bored'),
            array('No','Thanks ,but'),
        );
        // and another for evaluating
        $testing = array();
        array_push($testing,$answer);

        $tset = new TrainingSet(); // will hold the training documents
        $tok = new WhitespaceTokenizer(); // will split into tokens
        $ff = new DataAsFeatures(); // see features in documentation

        // ---------- Training ----------------
        foreach ($training as $d)
        {
            $tset->addDocument(
                $d[0], // class
                new TokensDocument(
                    $tok->tokenize($d[1]) // The actual document
                )
            );
        }

        $model = new FeatureBasedNB(); // train a Naive Bayes model
        $model->train($ff,$tset);


        // ---------- Classification ----------------
        $cls = new MultinomialNBClassifier($ff,$model);
        $correct = 0;
        foreach ($testing as $d)
        {
            // predict if it is spam or ham
            $prediction = $cls->classify(
                array('Yes','No'), // all possible classes
                new TokensDocument(
                    $tok->tokenize($d) // The document
                )
            );
            return $prediction;
        }

    }
    public function classifyQuestion($answer){
        $training = array(
            array('Activity','I can swimming'),
            array('Activity','Can I play football'),
            array('Activity','I often play soccer'),
            array('Activity','You need go shopping at center'),
            array('Activity','Can I walk around park'),
            array('Activity','On the morning, I go to school'),
            array('Activity','I often go to work at the afternoon'),
            array('Address','T will go to NewYork'),
            array('Address','Forecast weather at Tokyo'),
            array('Address','I must go to hometown today'),
            array('Address','Weather at China is nice'),
            array('Address','Weather at HaNoi'),
            array('Address','Sky in Washington is very fresh'),
            array('Address','The weather in HoChiMinh is hot, is it?'),
            array('Address','provice country HaNoi VietNam HaiPhong HaiDuong NamDinh ThaiBinh DaNang'),
            array('Address','I will go DaNang at weekend'),
            array('Address','I will go HaiPhong'),
            array('Address','Go DangNang'),
            array('Address','Go HaiDuong')
        );
        // and another for evaluating
        $testing = array();
        array_push($testing,$answer);

        $tset = new TrainingSet(); // will hold the training documents
        $tok = new WhitespaceTokenizer(); // will split into tokens
        $ff = new DataAsFeatures(); // see features in documentation

        // ---------- Training ----------------
        foreach ($training as $d)
        {
            $tset->addDocument(
                $d[0], // class
                new TokensDocument(
                    $tok->tokenize($d[1]) // The actual document
                )
            );
        }

        $model = new FeatureBasedNB(); // train a Naive Bayes model
        $model->train($ff,$tset);


        // ---------- Classification ----------------
        $cls = new MultinomialNBClassifier($ff,$model);
        $correct = 0;
        foreach ($testing as $d)
        {
            // predict if it is spam or ham
            $prediction = $cls->classify(
                array('Activity','Address'), // all possible classes
                new TokensDocument(
                    $tok->tokenize($d) // The document
                )
            );
            return $prediction;
        }
        return "NO valid";
    }
    public function continueOrStop($answer){
        $training = array(
            array('Yes','Yes'),
            array('Yes','Ok I want to ask'),
            array('Yes','Yes I need'),
            array('No','No thanks'),
            array('No','No I dont'),
            array('No','No see you again'),
        );
        // and another for evaluating
        $testing = array();
        array_push($testing,$answer);

        $tset = new TrainingSet(); // will hold the training documents
        $tok = new WhitespaceTokenizer(); // will split into tokens
        $ff = new DataAsFeatures(); // see features in documentation

        // ---------- Training ----------------
        foreach ($training as $d)
        {
            $tset->addDocument(
                $d[0], // class
                new TokensDocument(
                    $tok->tokenize($d[1]) // The actual document
                )
            );
        }

        $model = new FeatureBasedNB(); // train a Naive Bayes model
        $model->train($ff,$tset);


        // ---------- Classification ----------------
        $cls = new MultinomialNBClassifier($ff,$model);
        $correct = 0;
        foreach ($testing as $d)
        {
            // predict if it is spam or ham
            $prediction = $cls->classify(
                array('Yes','No'), // all possible classes
                new TokensDocument(
                    $tok->tokenize($d) // The document
                )
            );
            return $prediction;
        }
        return "NO valid";
    }
}
